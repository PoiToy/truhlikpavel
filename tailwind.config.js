/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/*.html", 
    "./src/*.js", 
    "./src/img/*.{png,jpg,jpeg}"],
  theme: {
    extend: {},
  },
  plugins: [],
};
