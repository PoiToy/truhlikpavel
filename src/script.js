function getMain() {
  window.location.href = "index.html";
}

function getContact() {
  window.location.href = "contact.html";
}

function getGalery() {
  window.location.href = "galery.html";
}

function getOrderRequest() {
  window.location.href = "orderRequest.html";
}

function showRollingBar() {
  const rollingBar = document.getElementById("rolling-bar");
  rollingBar.classList.toggle("hidden");
}


fetch("navBar.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("navbar-placeHolder").innerHTML = text;
  });

class ImageGallery {
  constructor() {
    this.images = [
      "./img/bed1.jpg",
      "./img/bed2.jpg",
      "./img/kids_room1.jpg",
      "./img/kids_room2.jpg",
      "./img/bathroom.jpg",
      "./img/kitchen1.jpg",
      "./img/kitchen2.jpg",
      "./img/kitchen3.jpg",
      "./img/kitchen4.jpg",
      "./img/kitchen5.jpg",
      "./img/living_room.jpg"
    ];
  }

  displayGallery() {
    const galleryContainer = document.getElementById("image-gallery");

    this.images.forEach((imageUrl) => {
      const img = document.createElement("img");
      img.src = imageUrl;
      img.classList.add("w-32", "h-32", "m-2");
      img.addEventListener("click", () => {
        this.toggleFullScreen(img); // Pass the image element instead of image source
      });
      galleryContainer.appendChild(img);
    });
  }

  toggleFullScreen(image) {
    image.classList.toggle("w-auto");
    image.classList.toggle("h-auto");
    image.classList.toggle("max-w-full");
    image.classList.toggle("max-h-full");
    image.classList.toggle("absolute");
    image.classList.toggle("top-1/2");
    image.classList.toggle("left-1/2");
    image.classList.toggle("transform");
    image.classList.toggle("-translate-x-1/2");
    image.classList.toggle("-translate-y-1/2");
    image.classList.toggle("p-4");
    image.classList.toggle("z-30");
  }
}

// Initialize and display the gallery when the DOM content is loaded
document.addEventListener("DOMContentLoaded", () => {
  const gallery = new ImageGallery();
  gallery.displayGallery();
});